class Node

    attr_reader :name, :edges
    attr_accessor :tmark, :pmark

    def initialize(name)
        @name = name
        @tmark = false
        @pmark = false
        @edges = []
    end

    def add_edge(dest)
        @edges.push(dest)
    end

end

def visit(n, res)
    if n.pmark then return end
    if n.tmark then raise ArgumentError, "The graph is not a DAG." end
    n.tmark = true
    for m in n.edges do
        visit(m, res)
    end
    n.pmark = true
    res.unshift(n)
end

def toposort(g)
    res = []
    for _, n in g do
        visit(n, res) unless n.pmark
    end
    res
end

nodes = gets.split

graph = Hash.new
for n in nodes
    graph[n] = Node.new(n)
end

s = ""
loop do
    s = gets.split
    if s != ['end']
        graph[s[0]].add_edge(graph[s[1]])
    else
        break
    end
end

p toposort(graph).map &:name

